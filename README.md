# Google Codelab: Adding a Service Worker with sw-precache Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/sw-precache/index.html, this is a simple tutorial that demonstrates how to use the [sw-precache](https://github.com/GoogleChrome/sw-precache), this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need:
- [git](https://git-scm.com/)
- A webserver as per tutorial I'll be using the [python http module](https://www.python.org/)
- npm available when installing [Node.js](https://nodejs.org/en/)

## Installing

The install process can be done by typing the following on a terminal:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Service_Worker_Codelab.git
cd Service_Worker_Codelab/src
npm install
```

## Running
You'll have to use a webserver to run this example, if you have python 3 run the following command inside the app folder:

```
python -m http.server 3000
```

or in python 2 run the command:

```
python -m SimpleHTTPServer 3000
```

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Google Crhrome](https://github.com/GoogleChrome) available at: https://github.com/GoogleChrome/airhorn.git
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/sw-precache/index.html
